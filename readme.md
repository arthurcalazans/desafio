# Primeiro Acesso
Primeiramente terão que acessar o CLI do projeto, nele irão execultar o sql.php. Ali irá instalar o banco de dados no localhost, user root. Caso tenha um Usuário diferente, ou senha, ou porta diferente, altere no arquivo SQL.php e no arquivo /Models/Config.php

Terá que criar também um virutal host, se for no windows e se tiver usando o xampp, então é só acessar o arquivo **host** da pasta **C://windows/system32/drives/etc**
No final do arquivo coloque **127.0.0.1 web.jump**. Depois vá no arquivo **httpd-vhost.conf** na pasta **C://xampp/apache/conf/extra** e cole a seguinte linha:

```sh
<VirtualHost *:80>
    ServerAdmin ArthurCalazans
    DocumentRoot "C:\xampp\htdocs\webjump"
    ServerName web.jump
    ErrorLog "logs/Web-Jump.log"
    CustomLog "logs/Web-Jump.log" common
</VirtualHost>
```

Feito isso poderá rodar o projeto em web.jump/

Para acessar o sistema, é necessário fazer um pequeno cadastro, para fins de saber quem está logando.

# Dashboard
Terá um pequeno relátorio mostrando quantos produtos estão cadastrados, quantas categorias cadastradas, e quantos usuarios cadastrados.

# Catégorias
Na página de catégorias você terá um crud completo, poderá cadastrar, editar ou excluir. E também terá uma tabela dinâmica para listar as catégorias.

# Produtos
Na página de produtos você terá um crud completo, poderá cadastrar, editar ou excluir. E também terá uma tabela dinâmica para listar os produtos.

#CLI
Acessando o arquivo **excel.php** na raiz do projeto, porderá importar os arquivos que tiverem na pasta assets, 

o código que eu usei e funcionou:

# Importar Excel
- php C:\xampp\htdocs\webjump\excel.php C:\xampp\htdocs\webjump\assets\import

# Importar SQL
- php C:\xampp\htdocs\webjump\sql.php


# Considerações
- PHP 7.3.3;
- MYSQL 10.1.38-MariaDB;
- Algumas controller estão orientado a objeto e outras não, pois quis mostrar que sei trabalhar em qualquer condição;
- Não consegui fazer o teste automatizado, pois só trabalhei com testes automatizados no **chocolate** e **laravel**;
- Teria terminado na terça passada se eu tivesse utilizado o laravel;
- Usei o básico de JS e CSS pra fazer alguma coisa visual e legal;
- Qualquer dúvida estarei a disposição.