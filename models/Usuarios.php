<?php


class Usuarios
{
    private $key = '$2a$07$arthurcalazans.....$';

    public function contar()
    {
        $config = new Config;
        $mysqli = $config->conectar();

        $query = "SELECT 1 FROM cad_usuarios;";
        $result = $mysqli->query($query);

        return $result->num_rows;
    }

    public function buscar_todos()
    {
        $config = new Config;
        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_produtos;";
        $result = $mysqli->query($query);

        return $result;
    }

    public function cadastrar($dados)
    {
        $config = new Config;
        $mysqli = $config->conectar();

        $senha = crypt($dados['password'], $this->key);

        $query = "INSERT INTO cad_usuarios (usuario, senha, nome_usuario) VALUES ('" . $dados['username'] . "','" . $senha . "','" . $dados['nome'] . "');";
        $result = $mysqli->query($query);

        if ($result) {
            header('Location:' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/login');
        } else {
            var_dump($mysqli->error);
        }
    }

    public function fazer_login($dados)
    {
        $config = new Config;
        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_usuarios WHERE usuario like '" . $dados['username'] . "'";
        $result = $mysqli->query($query);
        if ($result->num_rows > 0) {

            $fetch = $result->fetch_object();

            if ($fetch->senha === crypt($dados['password'], $fetch->senha)) {
                $_SESSION['login'] = true;
                $_SESSION['nome'] = $fetch->nome_usuario;
                $_SESSION['usuario'] = $fetch->usuario;
                $this->log("Logando no sistema");
                header('Location:' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/');
            }
        } else {
            var_dump("usuario ou senha errado");
        }
    }

    public function log($acao, $id = null)
    {
        $config = new Config;
        $mysqli = $config->conectar();

        $http = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        $query = "INSERT INTO log_usuarios (usuario, acao, pagina, id, ip) VALUES ('" . $_SESSION['usuario'] . "','" . $acao . "','" . $http . "', '" . $id . "', '" . $_SERVER['REMOTE_ADDR'] . "');";
        $mysqli->query($query);
    }
}
