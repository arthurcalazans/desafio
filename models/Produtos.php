<?php


class Produtos
{
    public function contar()
    {
        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "SELECT 1 FROM cad_produtos;";
        $result = $mysqli->query($query);
        $usuarios->log("Contado Produtos");

        return $result->num_rows;
    }

    public function buscar_todos()
    {
        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_produtos;";
        $result = $mysqli->query($query);
        $usuarios->log("Listando Produtos");

        return $result->fetch_all();
    }

    public function buscar_categorias()
    {
        $config = new Config;

        $mysqli = $config->conectar();

        $query = "SELECT nome_categoria, id_produto FROM rel_produtos_categorias r inner join cad_categorias c on c.id_categoria = r.id_categoria";
        $result = $mysqli->query($query);
        $cate[1] = "";
        while ($c = $result->fetch_object()) {
            if (empty($cate[$c->id_produto])) {
                $cate[$c->id_produto] = $c->nome_categoria;
            } else {
                $cate[$c->id_produto] = $cate[$c->id_produto] . ', ' . $c->nome_categoria;
            }
        }

        return $cate;
    }

    public function checked()
    {
        $config = new Config;

        $mysqli = $config->conectar();

        $query = "SELECT * FROM rel_produtos_categorias r inner join cad_categorias c on c.id_categoria = r.id_categoria";
        $result = $mysqli->query($query);

        while ($c = $result->fetch_object()) {
            $cate[$c->id_categoria] = 'checked';
        }

        return $cate;
    }

    public function buscar($id)
    {
        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_produtos WHERE id_produto = '" . $id . "';";
        $result = $mysqli->query($query);

        $usuarios->log("Buscando Produto", $id);

        return $result->fetch_array();
    }

    public function cadastrar($dados)
    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "INSERT INTO cad_produtos (nome_produto, codigo_produto, preco, quantidade, descricao) VALUES ('" . $dados['nome_produto'] . "','" . $dados['cod_produto'] . "','" . number_format($dados['preco_produto'], 2, '.', ',') . "','" . $dados['qnt_produto'] . "','" . $dados['descricao'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $id = $mysqli->insert_id;
            foreach ($dados['categorias'] as $cate) {
                $query = "INSERT INTO rel_produtos_categorias (id_produto, id_categoria) VALUES ('" . $id . "','" . $cate . "');";
                $result = $mysqli->query($query);
                if (!$result) {
                    return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
                }
            }
            $usuarios->log("Cadastrando Produto", $id);

            return json_encode(["mensagem" => "Cadastro realizado com sucesso!", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }

    public function editar($dados)
    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "UPDATE cad_produtos SET nome_produto = '" . $dados['nome_produto'] . "', codigo_produto = '" . $dados['cod_produto'] . "', preco = '" . $dados['preco_produto'] . "', quantidade = '" . $dados['qnt_produto'] . "', descricao = '" . $dados['descricao'] . "' WHERE (id_produto = '" . $dados['editar'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $mysqli->query("DELETE FROM rel_produtos_categorias WHERE id_produto = '" . $dados['editar'] . "'");

            foreach ($dados['categorias'] as $cate) {
                $query = "INSERT INTO rel_produtos_categorias (id_produto, id_categoria) VALUES ('" . $dados['editar'] . "','" . $cate . "');";
                $result = $mysqli->query($query);
                if (!$result) {
                    return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
                }
            }
            $usuarios->log("Editando Produto", $dados['editar']);

            return json_encode(["mensagem" => "Atualização realizado com sucesso!", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }

    public function excluir($dados)

    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;

        $mysqli = $config->conectar();

        $query = "DELETE FROM cad_produtos WHERE (id_produto = '" . $dados['excluir'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $usuarios->log("Excluindo Produto", $dados['excluir']);

            return json_encode(["mensagem" => "Exclusão realizada com sucesso", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }
}
