
CREATE DATABASE `webjump2`;

CREATE TABLE `cad_usuarios` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='tablea de cadastro de usuarios';

CREATE TABLE `cad_categorias` (
  `id_categoria` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome_categoria` varchar(255) NOT NULL,
  `codigo_categoria` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_categoria`),
  UNIQUE KEY `codigo_categoria_UNIQUE` (`codigo_categoria`),
  UNIQUE KEY `nome_categoria_UNIQUE` (`nome_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COMMENT='tablea de cadastro de categorias';

CREATE TABLE `cad_produtos` (
  `id_produto` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `nome_produto` varchar(255) NOT NULL,
  `codigo_produto` varchar(255) NOT NULL,
  `preco` double(10,2) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `descricao` varchar(999) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1 COMMENT='tabela de cadastro de produtos';

CREATE TABLE `rel_produtos_categorias` (
  `id_rel_produtos_categorias` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) unsigned NOT NULL,
  `id_categoria` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_rel_produtos_categorias`),
  KEY `fk_produtos` (`id_produto`),
  KEY `fk_categorias` (`id_categoria`),
  CONSTRAINT `fk_categorias` FOREIGN KEY (`id_categoria`) REFERENCES `cad_categorias` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_produtos` FOREIGN KEY (`id_produto`) REFERENCES `cad_produtos` (`id_produto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1948 DEFAULT CHARSET=latin1 COMMENT='tabela de relacionamento de produtos com suas categorias';
