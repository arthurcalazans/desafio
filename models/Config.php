<?php
require M . '/usuarios.php';

class Config
{

    private $host = "localhost";
    private $user = "root";
    private $pass = "";
    private $db = "webjump";
    private $port = "3307";

    public function conectar()
    {
        $mysqli =  new mysqli($this->host, $this->user, $this->pass, $this->db, $this->port);
        if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
        return $mysqli;
    }
}


if (!isset($_SESSION['login']) && $page !== "login" && $page !== "cadastro") {
    header('Location:' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/login');
}
