<?php


class Categorias
{
    
    public function contar()
    {
        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "SELECT 1 FROM cad_categorias;";
        $result = $mysqli->query($query);

        $usuarios->log("Contado Categorias");
        

        return $result->num_rows;
    }

    public function buscar_todas()
    {
        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_categorias;";
        $result = $mysqli->query($query);

        $usuarios->log("Listando todas categorias");

        return $result->fetch_all();
    }
    public function buscar($id)
    {
        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "SELECT * FROM cad_categorias WHERE id_categoria = '" . $id . "';";
        $result = $mysqli->query($query);

        $usuarios->log("Buscando categorias", $id);
        return $result->fetch_array();
    }

    public function cadastrar($dados)
    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "INSERT INTO cad_categorias (nome_categoria, codigo_categoria) VALUES ('" . $dados['nome_categoria'] . "','" . $dados['cod_categoria'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $usuarios->log("Cadastrnado uma categoria", $mysqli->insert_id);
            return json_encode(["mensagem" => "Cadastro realizado com sucesso!", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }

    public function editar($dados)
    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "UPDATE cad_categorias SET nome_categoria = '" . $dados['nome_categoria'] . "',codigo_categoria = '" . $dados['cod_categoria'] . "' WHERE (id_categoria = '" . $dados['editar'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $usuarios->log("Editando uma categoria", $dados['editar']);
            return json_encode(["mensagem" => "Atualização realizado com sucesso!", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }
    public function excluir($dados)
    {
        header('Content-type: application/json');

        $config = new Config;
        $usuarios = new Usuarios;
        $mysqli = $config->conectar();

        $query = "DELETE FROM cad_categorias WHERE (id_categoria = '" . $dados['excluir'] . "');";

        $result = $mysqli->query($query);
        if ($result) {
            $usuarios->log("Excluindo uma categoria", $dados['excluir']);
            return json_encode(["mensagem" => "Exclusão realizada com sucesso", "tipo" => "success"]);
        }
        return json_encode(["mensagem" => $mysqli->error, "tipo" => "error"]);
    }
}
