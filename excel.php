<?php

include  "Vendor\PHPExcel-1.8\Classes\PHPExcel.php";
error_reporting(E_ERROR | E_PARSE);

echo ("\n\nEstamos começando a importação do arquivo: " . $_SERVER['argv'][1] . ".csv.\n");
echo "----------------------------------------------------------\n";
echo "Carregando...\n";

$host = "localhost";
$user = "root";
$pass = "";
$db = "webjump";
$port = "3307";
$mysqli =  new mysqli($host, $user, $pass, $db, $port);
if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());


$dir = __DIR__ . '/assets/';
$arquivo = $dir . basename($_SERVER['argv'][1] . '.csv');

$excel = PHPExcel_IOFactory::load($arquivo);

$dados = $excel->getActiveSheet()->toArray(null, true, true, true);

foreach ($dados as $linhas) {
    $conteudo = "";
    foreach ($linhas as $linha) {
        $conteudo .= $linha;
    }
    $vetor[] = explode(';', $conteudo);
    $temp[] = explode('|', explode(';', $conteudo)[5]);
}
unset($vetor[0]);


$query = "SELECT max(id_categoria) FROM cad_categorias";
$result = $mysqli->query($query);

$i = $result->fetch_array()[0] + 1;

foreach ($temp as $a1) {
    foreach ($a1 as $a2) {
        if (!empty($a2) && $a2 != "(no genres listed)" && $a2 != "categoria") {
            if (!in_array($a2, $b1)) {
                $b1[] = $a2;
            }
        }
    }
}
sort($b1);
foreach ($b1 as $cate) {
    $categorias[$cate] = $i;
    $cod = substr($cate, 0, 1) . str_pad($i, 4, '0', STR_PAD_LEFT);
    $query = "INSERT INTO cad_categorias (nome_categoria, codigo_categoria) VALUES ('" . $cate . "','" . $cod . "');";
    $mysqli->query($query);
    $i++;
}


foreach ($vetor as $prod) {
    $query = "INSERT INTO cad_produtos (nome_produto, codigo_produto, preco, quantidade, descricao) VALUES ('" . $prod[0] . "','" . $prod[1] . "','" . number_format($prod[4], 2, '.', ',') . "','" . $prod[3] . "','" . $prod[2] . "');";
    $mysqli->query($query);
    $id = $mysqli->insert_id;
    $temp = explode('|', $prod[5]);
    foreach ($temp as $cate) {
        if (isset($categorias[$cate])) {
            $query = "INSERT INTO rel_produtos_categorias (id_produto, id_categoria) VALUES ('" . $id . "','" . $categorias[$cate] . "');";
            $mysqli->query($query);
        }
    }
}
echo "----------------------------------------------------------\n";
echo "Finalizado\n";
