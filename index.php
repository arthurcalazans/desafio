<?php

session_start();

defined('V') or define('V', __DIR__ . '/assets/views/');
defined('C') or define('C', __DIR__ . '/controllers/');
defined('M') or define('M', __DIR__ . '/models/');

function url($string)
{
    return ($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/assets/' . $string);
}

$scripts[] = "jquery.min.js";
$scripts[] = "plugins.min.js";

$stylesheets[] = "vendors.min.css";
$stylesheets[] = "materialize.min.css";
$stylesheets[] = "style.min.css";
$stylesheets[] = "custom.css";

if (isset($_GET['page'])) {    
    $page = strtolower($_GET['page']);
} else {
    $page = "Dashboard";
}
if (isset($_GET['acao'])) {
    $acao = strtolower($_GET['acao']);
} else {
    unset($acao);
}
if (isset($_GET['id'])) {
    $id = strtolower($_GET['id']);
} else {
    unset($id);
}

if (file_exists(C . $page . '/index.php')) {
    require M . 'Config.php';
    require C . $page . '/index.php';
} else {
    $erro = "404";
    require C . 'erro/index.php';
}
