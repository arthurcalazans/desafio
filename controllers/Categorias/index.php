<?php

require M . '/categorias.php';
$categorias = new Categorias;

if (isset($_POST['cadastrar'])) {

    echo $categorias->cadastrar($_POST);
    exit(0);
} elseif (isset($_POST['editar'])) {

    echo $categorias->editar($_POST);
    exit(0);
} elseif (isset($_POST['excluir'])) {

    echo $categorias->excluir($_POST);
    exit(0);
}

$fetch = $categorias->buscar_todas();

if (empty($acao)) {
    $scripts[] = "jquery.dataTables.min.js";
    $scripts[] = "dataTables.responsive.min.js";
    $scripts[] = "dataTables.select.min.js";
    $scripts[] = "data-tables.js";
    $stylesheets[] = "jquery.dataTables.min.css";
    $stylesheets[] = "responsive.dataTables.min.css";
    $stylesheets[] = "select.dataTables.min.css";
    $stylesheets[] = "data-tables.css";
} else {
    if ($acao == "editar") {
        $cate = $categorias->buscar($id);
    }
}
$scripts[] = "sweetalert.min.js";
$stylesheets[] = "sweetalert.css";
$scripts[] = "script-categorias.js";

include V . '/template.html';
