<?php

require M . '/produtos.php';

$produtos = new Produtos;

if (isset($_POST['cadastrar'])) {

    echo $produtos->cadastrar($_POST);
    exit(0);
} elseif (isset($_POST['editar'])) {

    echo $produtos->editar($_POST);
    exit(0);
} elseif (isset($_POST['excluir'])) {

    echo $produtos->excluir($_POST);
    exit(0);
}

if (empty($acao)) {
    $fetch = $produtos->buscar_todos();
    $cate = $produtos->buscar_categorias();

    $scripts[] = "jquery.dataTables.min.js";
    $scripts[] = "dataTables.responsive.min.js";
    $scripts[] = "dataTables.select.min.js";
    $scripts[] = "data-tables.js";
    $stylesheets[] = "jquery.dataTables.min.css";
    $stylesheets[] = "responsive.dataTables.min.css";
    $stylesheets[] = "select.dataTables.min.css";
    $stylesheets[] = "data-tables.css";
} else {
    if ($acao === "editar") {
        $prod = $produtos->buscar($id);
        $c = $produtos->checked();
    }
    require M . '/categorias.php';
    $categorias = new Categorias;
    $qtd = $categorias->contar();
    $fetch = $categorias->buscar_todas();
}

$scripts[] = "jquery.mask.min.js";
$scripts[] = "script-produtos.js";
$scripts[] = "sweetalert.min.js";
$stylesheets[] = "sweetalert.css";

include V . '/template.html';
