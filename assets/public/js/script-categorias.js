$(document).ready(function () {

    $("form").submit(function (e) {
        e.preventDefault();

        var dados = $(this).serializeArray();

        $.post("/categorias", dados, function (data) {
            swal({
                title: data['mensagem'],
                icon: data['tipo'],
                dangerMode: true
            }).then(function () {
                if (document.location.pathname.split("/")[2] === 'cadastrar') {
                    $("form")[0].reset();
                } else {
                    document.location.href = '/categorias';
                }
            });
        }).fail(function (data) {
            swal({
                title: 'Erro',
                message: data.statusText,
                icon: 'error'
            })
        });
        return false;
    });

    $("#excluir").click(function () {
        dados = { excluir: $("#excluir").val() };

        $.post("/categorias", dados, function (data) {
            swal({
                title: 'Categoria excluida com sucesso',
                icon: 'success'
            }).then(function (teste) {
                document.location.href = '/categorias';
            });
        }).fail(function (data) {
            swal({
                title: 'Erro',
                message: data.statusText,
                icon: 'error'
            })
        });
        return false;
    });

});


