$(document).ready(function () {

    $("#form-cadastro-produto").submit(function (e) {
        e.preventDefault();

        var dados = $(this).serializeArray();

        if (dados.length === 5) {

        } else {
            swal({
                title: "Sem Categoria",
                text: "Você não pode cadastrar um produto sem categoria",
                icon: 'warning',
                dangerMode: true,
                buttons: {
                    cancel: 'Sair',
                    delete: 'Cadastrar Categoria'
                }
            }).then(function () {
                if (teste) {
                    location.href = "/categorias/cadastrar";
                }
            });
        }

    });


    $("form").submit(function (e) {
        e.preventDefault();

        var dados = $(this).serializeArray();

        $.post("/produtos", dados, function (data) {
            swal({
                title: data['mensagem'],
                icon: data['tipo'],
                dangerMode: true
            }).then(function () {
                if (document.location.pathname.split("/")[2] === 'cadastrar') {
                    $("form")[0].reset();
                } else {
                    document.location.href = '/produtos';
                }
            });
        }).fail(function (data) {
            swal({
                title: 'Erro',
                message: data.statusText,
                icon: 'error'
            })
        });
        return false;
    });

    $("#excluir").click(function () {
        dados = { excluir: $("#excluir").val() };

        $.post("/produtos", dados, function (data) {
            swal({
                title: 'Categoria excluida com sucesso',
                icon: 'success'
            }).then(function (teste) {
                document.location.href = '/produtos';
            });
        }).fail(function (data) {
            swal({
                title: 'Erro',
                message: data.statusText,
                icon: 'error'
            })
        });
        return false;
    });

    $("#preco_produto").mask('000.000.000.000.000,00', {reverse: true});
});